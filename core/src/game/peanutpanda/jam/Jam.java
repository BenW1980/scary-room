package game.peanutpanda.jam;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import game.peanutpanda.jam.assetmanager.AssetLoader;
import game.peanutpanda.jam.controller.KeyBoard;
import game.peanutpanda.jam.controller.Mouse;
import game.peanutpanda.jam.funStuffs.Text;
import game.peanutpanda.jam.screens.IntroScreen;

public class Jam extends Game {

    public AssetLoader assetLoader;
    public OrthographicCamera camera;
    public Viewport viewport;
    public KeyBoard keyBoard;
    public Mouse mouse;
    public Stage stage;
    public Text text;
    public BitmapFont monkeyRed;
    public BitmapFont monkeyBlue;
    private BitmapFont monkey;

    @Override
    public void create() {
        this.assetLoader = new AssetLoader();
        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);
        this.keyBoard = new KeyBoard();
        this.mouse = new Mouse();
        this.stage = new Stage();
        stage.setViewport(viewport);

        InputMultiplexer multiplexer = new InputMultiplexer();
        Gdx.input.setInputProcessor(multiplexer);
        multiplexer.addProcessor(this.keyBoard);
        multiplexer.addProcessor(this.mouse);
        multiplexer.addProcessor(this.stage);

        this.assetLoader.loadFontHandling();
        this.assetLoader.loadImages();
        this.assetLoader.loadMusic();
        this.assetLoader.loadSounds();
        this.assetLoader.manager.finishLoading();
        this.monkeyRed = assetLoader.manager.get("monkeyRed.ttf", BitmapFont.class);
        this.monkeyBlue = assetLoader.manager.get("monkeyBlue.ttf", BitmapFont.class);
        this.monkey = assetLoader.manager.get("monkey.ttf", BitmapFont.class);
        this.text = new Text(stage);

        this.setScreen(new IntroScreen(this));

    }
}
