package game.peanutpanda.jam.assetmanager;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.utils.Array;


public class AssetLoader {

    public final AssetManager manager = new AssetManager();

    public void loadFontHandling() {

        FileHandleResolver resolver = new InternalFileHandleResolver();
        manager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        manager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));

        FreetypeFontLoader.FreeTypeFontLoaderParameter monkeyRed = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        monkeyRed.fontFileName = "fonts/monkey.ttf";
        monkeyRed.fontParameters.size = 15;
        monkeyRed.fontParameters.color = Color.WHITE;
        monkeyRed.fontParameters.borderWidth = 1;
        monkeyRed.fontParameters.borderColor = Color.RED;
        manager.load("monkeyRed.ttf", BitmapFont.class, monkeyRed);

        FreetypeFontLoader.FreeTypeFontLoaderParameter monkeyBlue = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        monkeyBlue.fontFileName = "fonts/monkey.ttf";
        monkeyBlue.fontParameters.size = 15;
        monkeyBlue.fontParameters.color = Color.WHITE;
        monkeyBlue.fontParameters.borderWidth = 1;
        monkeyBlue.fontParameters.borderColor = Color.BLUE;
        manager.load("monkeyBlue.ttf", BitmapFont.class, monkeyBlue);

        FreetypeFontLoader.FreeTypeFontLoaderParameter monkey = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        monkey.fontFileName = "fonts/monkey.ttf";
        monkey.fontParameters.size = 15;
        monkey.fontParameters.color = Color.WHITE;
        monkey.fontParameters.borderWidth = 1;
        monkey.fontParameters.borderColor = Color.BLACK;
        manager.load("monkey.ttf", BitmapFont.class, monkey);
    }

    public void loadImages() {
        manager.load("images/room.png", Texture.class);
        manager.load("images/shadow.png", Texture.class);
        manager.load("images/painting.png", Texture.class);
        manager.load("images/clock.png", Texture.class);
        manager.load("images/hourHandle.png", Texture.class);
        manager.load("images/minuteHandle.png", Texture.class);
        manager.load("images/fearBar.png", Texture.class);
        manager.load("images/lightBar.png", Texture.class);
        manager.load("images/hudBack.png", Texture.class);
        manager.load("images/frame.png", Texture.class);
        manager.load("images/roomTeeth.png", Texture.class);
        manager.load("images/snow.png", Texture.class);
        manager.load("images/snow2.png", Texture.class);
        manager.load("images/snow3.png", Texture.class);
        manager.load("images/skull.png", Texture.class);
        manager.load("images/black.png", Texture.class);
    }

    public void loadSounds() {
        manager.load("sounds/outro.mp3", Sound.class);
        manager.load("sounds/creak2.wav", Sound.class);
        manager.load("sounds/doorclose.wav", Sound.class);
        manager.load("sounds/doorslam.wav", Sound.class);
        manager.load("sounds/groan.wav", Sound.class);
        manager.load("sounds/knocking.wav", Sound.class);
        manager.load("sounds/stairs.wav", Sound.class);
        manager.load("sounds/thud.wav", Sound.class);
        manager.load("sounds/thud2.wav", Sound.class);
        manager.load("sounds/thuds.wav", Sound.class);
        manager.load("sounds/mouth.wav", Sound.class);
    }

    public Sound outroSound() {
        return manager.get("sounds/outro.mp3", Sound.class);
    }

    public Array<Sound> sounds() {

        Array<Sound> sounds = new Array<>();

        sounds.addAll(
                manager.get("sounds/creak2.wav", Sound.class),
                manager.get("sounds/doorclose.wav", Sound.class),
                manager.get("sounds/doorslam.wav", Sound.class),
                manager.get("sounds/groan.wav", Sound.class),
                manager.get("sounds/knocking.wav", Sound.class),
                manager.get("sounds/stairs.wav", Sound.class),
                manager.get("sounds/thud.wav", Sound.class),
                manager.get("sounds/thud2.wav", Sound.class),
                manager.get("sounds/thuds.wav", Sound.class),
                manager.get("sounds/mouth.wav", Sound.class)

        );

        return sounds;

    }


    public void loadMusic() {
        manager.load("music/amb.ogg", Music.class);
    }

    public Music introMusic() {
        return manager.get("music/amb.ogg", Music.class);
    }

    public Texture black() {
        return manager.get("images/black.png", Texture.class);
    }

    public Texture skull() {
        return manager.get("images/skull.png", Texture.class);
    }

    public Texture snow() {
        return manager.get("images/snow.png", Texture.class);
    }

    public Texture snow2() {
        return manager.get("images/snow2.png", Texture.class);
    }

    public Texture snow3() {
        return manager.get("images/snow3.png", Texture.class);
    }

    public Texture roomTeeth() {
        return manager.get("images/roomTeeth.png", Texture.class);
    }

    public Texture frame() {
        return manager.get("images/frame.png", Texture.class);
    }

    public Texture fearBar() {
        return manager.get("images/fearBar.png", Texture.class);
    }

    public Texture lightBar() {
        return manager.get("images/lightBar.png", Texture.class);
    }

    public Texture hudBack() {
        return manager.get("images/hudBack.png", Texture.class);
    }

    public Texture room() {
        return manager.get("images/room.png", Texture.class);
    }

    public Texture shadow() {
        return manager.get("images/shadow.png", Texture.class);
    }

    public Texture painting() {
        return manager.get("images/painting.png", Texture.class);
    }

    public Texture clock() {
        return manager.get("images/clock.png", Texture.class);
    }

    public Texture hourHandle() {
        return manager.get("images/hourHandle.png", Texture.class);
    }

    public Texture minuteHandle() {
        return manager.get("images/minuteHandle.png", Texture.class);
    }


}
