package game.peanutpanda.jam.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Mouse implements InputProcessor {

    private Pixmap pm;
    private Vector3 position;
    private Vector2 location;
    private final int SIZE = 32;
    private boolean button1Down, button2Down;

    public Mouse() {
        this.location = new Vector2(0, 0);
        this.setDefaultCursor("");
    }

    public void update(Viewport viewport) {
        position = new Vector3(location, 0);
        viewport.getCamera().unproject(position,
                viewport.getScreenX(),
                viewport.getScreenY(),
                viewport.getScreenWidth(),
                viewport.getScreenHeight());
    }

    public void setDefaultCursor(String path) {
        if (!path.isEmpty()) {
            pm = new Pixmap(Gdx.files.internal(path));
            Gdx.graphics.setCursor(Gdx.graphics.newCursor(pm, SIZE, SIZE));
            pm.dispose();
        }
    }


    @Override
    public boolean keyDown(int keycode) {
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (button == 0) {
            button1Down = true;
        } else if (button == 1) {
            button2Down = true;
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (button == 0) {
            button1Down = false;
        } else if (button == 1) {
            button2Down = false;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        location.x = screenX;
        location.y = screenY;
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        location.x = screenX;
        location.y = screenY;
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public boolean isLeftClicked() {
        return button1Down;
    }

    public boolean isRightClicked() {
        return button2Down;
    }

    public Vector2 getLocation() {
        return location;
    }
}