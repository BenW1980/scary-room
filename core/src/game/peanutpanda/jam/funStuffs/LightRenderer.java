package game.peanutpanda.jam.funStuffs;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;


public class LightRenderer {

    private RayHandler rayHandler;
    private OrthographicCamera camera;
    private Color darkColor;
    private Color lightColor;
    private World lightWorld;
    private boolean lightsOn;
    private Array<PointLight> lightPoints;

    public LightRenderer(OrthographicCamera camera) {
        this.camera = camera;
        lightWorld = new World(new Vector2(), true);
        this.darkColor = new Color(0.03f, 0.03f, 0.1f, 1.0f);
        this.lightColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.lightsOn = true;
        this.lightPoints = new Array<>();
        RayHandler.useDiffuseLight(true);
        this.rayHandler = new RayHandler(lightWorld);

    }

    public void lightsOff() {
        this.lightsOn = false;
        this.rayHandler.setAmbientLight(darkColor);
    }

    public void lightsOn() {
        this.lightsOn = true;
        this.rayHandler.setAmbientLight(lightColor);
    }

    public void extinguish() {
        rayHandler.removeAll();
        this.lightPoints.clear();
        this.lightPoints = new Array<>();
    }

    public void createLightPoint(float x, float y, Color color, float size) {
        lightPoints.add(new PointLight(rayHandler, 128, color, size, x, y));

    }

    public void render() {
        this.rayHandler.setCombinedMatrix(this.camera.combined);
        this.rayHandler.updateAndRender();
    }


    public void resize(Viewport viewport) {
        this.rayHandler.useCustomViewport(
                viewport.getScreenX(),
                viewport.getScreenY(),
                viewport.getScreenWidth(),
                viewport.getScreenHeight());
    }

    public void dispose() {
        this.lightWorld.dispose();
        this.rayHandler.dispose();

    }

    public boolean areLightsOn() {
        return lightsOn;
    }

    public Array<PointLight> getLightPoints() {
        return lightPoints;
    }

    public void setLightPoints(Array<PointLight> lightPoints) {
        this.lightPoints = lightPoints;
    }
}