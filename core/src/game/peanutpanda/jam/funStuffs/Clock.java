package game.peanutpanda.jam.funStuffs;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import game.peanutpanda.jam.assetmanager.AssetLoader;

public class Clock {

    private float dayTime;
    private Sprite clockBack;
    private Sprite hourHandle;
    private Sprite minuteHandle;
    private Stage stage;
    private float speed;
    private boolean reverse;

    public Clock(AssetLoader assetLoader, Stage stage) {
        this.stage = stage;
        this.dayTime = 6;

        this.clockBack = new Sprite(assetLoader.clock());
        this.clockBack.setBounds(135, 445, 110, 110);

        this.hourHandle = new Sprite(assetLoader.hourHandle());
        this.hourHandle.setBounds(135, 445, 110, 110);
        this.hourHandle.setOriginCenter();

        this.minuteHandle = new Sprite(assetLoader.minuteHandle());
        this.minuteHandle.setBounds(135, 445, 110, 110);
        this.minuteHandle.setOriginCenter();

    }

    public void drawCycle(float delta) {
        advanceTime(delta);
        calculateRotations();
        drawTime();
    }


    private void calculateRotations() {
        minuteHandle.setRotation(-(dayTime * 360));
        hourHandle.setRotation(minuteHandle.getRotation() / 12);
    }

    private void drawTime() {
        clockBack.draw(stage.getBatch());
        minuteHandle.draw(stage.getBatch());
        hourHandle.draw(stage.getBatch());
    }

    private void advanceTime(float delta) {
        if (reverse) {
            this.dayTime += delta / -speed;
        } else {
            this.dayTime += delta / speed;
        }

    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }
}