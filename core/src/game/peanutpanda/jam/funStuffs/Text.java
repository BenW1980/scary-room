package game.peanutpanda.jam.funStuffs;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Text {
    private float textDrawLength, textSpeed;
    private Stage stage;

    public Text(Stage stage) {
        this.stage = stage;
        textDrawLength = 0.0f;
        textSpeed = 0.4f;
    }

    public void print(BitmapFont font, String textToPrint, int x, int y) {
        font.draw(stage.getBatch(), textToPrint, x, y);
    }

    public void printDelay(BitmapFont font, String textToPrint, int x, int y) {

        if ((int) textDrawLength < textToPrint.length()) {
            textDrawLength += textSpeed;
        }
        font.draw(stage.getBatch(), (textToPrint.substring(0, (int) textDrawLength)), x, y);
    }
}
