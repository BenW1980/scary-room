package game.peanutpanda.jam.funStuffs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import game.peanutpanda.jam.ScreenSize;
import game.peanutpanda.jam.assetmanager.AssetLoader;

public class Event {

    private AssetLoader assetLoader;
    private Texture shadow, roomTeethTexture;
    private Stage stage;
    private boolean happening, paintingTurned, shadowVisible, fastTime,
            lightOrbs, screenShaking, roomTeeth, snow;
    private Clock clock;
    private LightRenderer lightRenderer;
    private float timePassed;
    private Array<Texture> snowArray;

    public Event(AssetLoader assetLoader, Stage stage, LightRenderer lightRenderer) {
        this.assetLoader = assetLoader;
        this.stage = stage;
        this.lightRenderer = lightRenderer;
        this.clock = new Clock(assetLoader, stage);
        this.shadow = assetLoader.shadow();
        this.roomTeethTexture = assetLoader.roomTeeth();
        this.snowArray = new Array<>();
        this.snowArray.addAll(assetLoader.snow(), assetLoader.snow2(), assetLoader.snow3());
    }

    public void reset() {
        happening = false;
        paintingTurned = false;
        fastTime = false;
        shadowVisible = false;
        lightOrbs = false;
        screenShaking = false;
        roomTeeth = false;
        snow = false;
        timePassed = 0;
    }

    public void getRandomEvent() {
        if (!happening) {
            int random = MathUtils.random(1, 10);
            assetLoader.sounds().get(random - 1).play();

            switch (random) {
                case 1:
                    shadowVisible = true;
                    break;
                case 2:
                    paintingTurned = true;
                    break;
                case 3:
                    fastTime = true;
                    break;
                case 4:
                    lightOrbs = true;
                    break;
                case 5:
                    screenShaking = true;
                    break;
                case 6:
                    roomTeeth = true;
                    break;
                case 7:
                    snow = true;
                    break;
            }
            happening = true;
        }

    }

    public void createLightOrbs(float delta) {
        timePassed += delta * 1000;
        if (lightOrbs && lightRenderer.getLightPoints().size < 100) {
            if (timePassed > 0.01f) {
                timePassed = 0;
                lightRenderer.createLightPoint(
                        MathUtils.random(10, ScreenSize.WIDTH.getSize()),
                        MathUtils.random(10, ScreenSize.HEIGHT.getSize()),
                        new Color(MathUtils.random(0.9f, 0.9f),
                                MathUtils.random(0.9f, 0.9f),
                                MathUtils.random(0.9f, 0.9f), 1),
                        MathUtils.random(10, 10));
            }
        }
        if (lightRenderer.getLightPoints().size > 99) {
            lightRenderer.extinguish();
            lightOrbs = false;
        }
    }

    public void snow(float delta) {
        timePassed += delta * 1000;
        if (snow) {
            if (timePassed > 0.1f) {
                int random = MathUtils.random(0, snowArray.size - 1);
                stage.getBatch().draw(snowArray.get(random), 112, 335, 88, 40);
                timePassed = 0;
            }

        }
    }

    public void drawRoomTeeth() {
        if (roomTeeth) {
            stage.getBatch().draw(roomTeethTexture, 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        }
    }

    public void drawShadow(float x, float y, float width, float height) {
        if (shadowVisible) {
            stage.getBatch().draw(shadow, x, y, width, height);
        }
    }

    public void shakeScreen(ScreenShake screenShake) {
        if (screenShaking) {
            screenShake.shake(0.3f, 1, 35, 25, true);
        }
    }

    public void showTime(float delta) {
        if (fastTime) {
            clock.setSpeed(0.15f);
        } else {
            clock.setSpeed(2000);
        }

        clock.drawCycle(delta);
    }

    public boolean isPaintingTurned() {
        return paintingTurned;
    }

}
