package game.peanutpanda.jam.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.Viewport;
import game.peanutpanda.jam.*;
import game.peanutpanda.jam.assetmanager.AssetLoader;
import game.peanutpanda.jam.controller.KeyBoard;
import game.peanutpanda.jam.controller.Mouse;
import game.peanutpanda.jam.funStuffs.Event;
import game.peanutpanda.jam.funStuffs.LightRenderer;
import game.peanutpanda.jam.funStuffs.ScreenShake;
import game.peanutpanda.jam.funStuffs.Text;

public abstract class GameScreen implements Screen {

    protected Jam game;
    protected OrthographicCamera camera;
    protected Viewport viewport;
    protected KeyBoard keyBoard;
    protected Mouse mouse;
    protected Text text;
    protected Table rootTable;
    protected Stage stage;
    protected AssetLoader assetLoader;
    protected LightRenderer lightRenderer;
    protected Event event;
    protected ScreenShake screenShake;

    protected long startTime;
    protected long endTime;


    GameScreen(Jam game) {
        this.game = game;
        this.camera = game.camera;
        this.viewport = game.viewport;
        this.stage = game.stage;
        this.keyBoard = game.keyBoard;
        this.mouse = game.mouse;
        this.text = game.text;
        this.assetLoader = game.assetLoader;
        this.lightRenderer = new LightRenderer(camera);
        this.screenShake = new ScreenShake();
        this.event = new Event(assetLoader, stage, lightRenderer);

        rootTable = new Table();

        rootTable.setFillParent(true);

        stage.addActor(rootTable);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1f);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        delta = Math.min(delta, 0.1f);

        camera.update();
        mouse.update(viewport);
        screenShake.update(delta, camera);
        stage.act();
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
        lightRenderer.resize(viewport);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        game.assetLoader.manager.dispose();
        lightRenderer.dispose();
    }
}
