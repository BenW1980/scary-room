package game.peanutpanda.jam.screens;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import game.peanutpanda.jam.Jam;
import game.peanutpanda.jam.ScreenSize;

public class GameOverScreen extends GameScreen {

    private Button playAgain;
    private float timePassed;
    private long secondsLasted;

    GameOverScreen(final Jam game, long time) {
        super(game);

        secondsLasted = time;

        this.playAgain = new Button();
        this.playAgain.setStyle(new Button.ButtonStyle());
        this.playAgain.setSize(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.playAgain.setPosition(0, 0);

        this.playAgain.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                if (timePassed > 15) {
                    game.setScreen(new RoomScreen(game));
                }
            }
        });
        rootTable.addActor(this.playAgain);
        assetLoader.outroSound().play();

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        super.render(delta);
        delta = Math.min(delta, 0.1f);
        timePassed += delta * 10;

        stage.getBatch().begin();

        if (timePassed < 10) {
            int random = MathUtils.random(1, 2);
            if (random == 1) {
                stage.getBatch().draw(assetLoader.black(), 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
            } else {
                stage.getBatch().draw(assetLoader.fearBar(), 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
            }
            stage.getBatch().draw(assetLoader.skull(), 122, 0, 780, 943);
        } else if (timePassed > 10) {
            text.print(game.monkeyRed, outroTxt(), 300, ScreenSize.HEIGHT.getSize() - 300);
        }

        stage.getBatch().end();
    }

    private String outroTxt() {
        return "YOU HAVE DIED OF FRIGHT\n\n\nYou lasted " + secondsLasted + " seconds." +
                "\n\n\n   Click to play again.";
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
