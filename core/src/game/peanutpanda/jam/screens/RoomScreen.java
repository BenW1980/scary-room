package game.peanutpanda.jam.screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import game.peanutpanda.jam.Jam;
import game.peanutpanda.jam.ScreenSize;

public class RoomScreen extends GameScreen {

    private Button lightButton;
    private Sprite painting;
    private Sprite fearBar;
    private Sprite lightBar;
    private Texture hudBack;
    private Texture frame;

    private final int maxWidth = ScreenSize.WIDTH.getSize() - 170;

    public RoomScreen(Jam game) {
        super(game);

        this.hudBack = assetLoader.hudBack();
        this.frame = assetLoader.frame();

        this.painting = new Sprite(assetLoader.painting());
        this.painting.setBounds(50, 600, 284, 132);
        this.painting.setOriginCenter();

        this.fearBar = new Sprite(assetLoader.fearBar());
        this.fearBar.setBounds(90, 50, 0, 20);
        this.lightBar = new Sprite(assetLoader.lightBar());
        this.lightBar.setBounds(90, 20, maxWidth, 20);

        this.lightButton = new Button();
        this.lightButton.setStyle(new Button.ButtonStyle());
        this.lightButton.setSize(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.lightButton.setPosition(0, 0);

        this.lightButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {

                if (lightRenderer.areLightsOn()) {
                    lightRenderer.lightsOff();
                } else {
                    lightRenderer.lightsOn();
                }
            }
        });
        this.lightButton.setFillParent(true);
        rootTable.addActor(this.lightButton);

        lightRenderer.lightsOn();

        startTime = System.currentTimeMillis();

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        super.render(delta);
        delta = Math.min(delta, 0.1f);


        event.shakeScreen(screenShake);

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.room(), 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        darknessHappenings(delta);
        painting.draw(stage.getBatch());
        event.drawShadow(390, 185, 100, 300);
        event.drawRoomTeeth();
        event.createLightOrbs(delta);
        event.showTime(delta);
        stage.getBatch().end();
        lightRenderer.render();

        stage.getBatch().begin();
        stage.getBatch().draw(hudBack, 0, 0, ScreenSize.WIDTH.getSize(), 80);
        fearBar.draw(stage.getBatch());
        lightBar.draw(stage.getBatch());
        stage.getBatch().draw(assetLoader.skull(), maxWidth + 95, 45, 25, 30);
        stage.getBatch().draw(frame, 90, 50, maxWidth, 20);
        stage.getBatch().draw(frame, 90, 20, maxWidth, 20);
        event.snow(delta);
        text.print(game.monkeyRed, "FEAR", 20, 68);
        text.print(game.monkeyBlue, "LIGHT", 20, 38);
        stage.getBatch().end();

        endTime = ((System.currentTimeMillis() - startTime) / 1000);

        System.out.println(endTime);

    }

    private void darknessHappenings(float delta) {

        if (lightBar.getWidth() < 1) {
            lightRenderer.lightsOff();
        }

        if (fearBar.getWidth() > maxWidth - 1) {
            game.setScreen(new GameOverScreen(game, endTime));
        }

        painting.setRotation(event.isPaintingTurned() ? 30 : 0);

        if (!lightRenderer.areLightsOn()) {
            event.getRandomEvent();
            if (fearBar.getWidth() < maxWidth) {
                fearBar.setSize(fearBar.getWidth() + delta * 100, fearBar.getHeight());
            }
            if (lightBar.getWidth() < maxWidth) {
                lightBar.setSize(lightBar.getWidth() + delta * 70, lightBar.getHeight());
            }
        } else {
            lightRenderer.extinguish();
            event.reset();

            if (fearBar.getWidth() > 0) {
                fearBar.setSize(fearBar.getWidth() - delta * 70, fearBar.getHeight());
            }
            if (lightBar.getWidth() > 0) {
                lightBar.setSize(lightBar.getWidth() - delta * 120, lightBar.getHeight());
            }
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
