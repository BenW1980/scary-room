package game.peanutpanda.jam.screens;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import game.peanutpanda.jam.Jam;
import game.peanutpanda.jam.ScreenSize;

public class IntroScreen extends GameScreen {

    private Button play;
    private float timePassed;
    private boolean ready;

    public IntroScreen(final Jam game) {
        super(game);


        this.play = new Button();
        this.play.setStyle(new Button.ButtonStyle());
        this.play.setSize(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.play.setPosition(0, 0);

        this.play.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                if (timePassed > 100) {
                    game.setScreen(new RoomScreen(game));
                }

            }
        });
        rootTable.addActor(this.play);
        assetLoader.introMusic().play();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        super.render(delta);
        delta = Math.min(delta, 0.1f);
        timePassed += delta * 10;

        stage.getBatch().begin();

        text.printDelay(game.monkeyRed, introText(), 100, ScreenSize.HEIGHT.getSize() - 200);

        stage.getBatch().end();

    }

    private String introText() {

        return "Clicking the mouse button will toggle the lights.\n\n" +
                "When the lights are off, the FEAR meter will rise.\n\n" +
                "When the lights are on, the FEAR meter will drop down.\n\n" +
                "When your FEAR meter is full, you will die of fright.\n\n\n" +
                "The LIGHT meter shows you how much power the lights have.\n\n" +
                "The LIGHT meter can be replenished by toggling the lights.\n\n\n" +
                "Be careful, the night is dark and full of terrors...\n\n\n\n" +
                "Turn up the brightness and the volume.\n\nClick when ready.";

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
