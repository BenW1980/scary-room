package game.peanutpanda.jam;

public enum ScreenSize {

    WIDTH(1024), HEIGHT(840);

    private final int size;

    ScreenSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

}