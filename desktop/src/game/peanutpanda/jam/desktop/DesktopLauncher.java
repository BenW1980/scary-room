package game.peanutpanda.jam.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import game.peanutpanda.jam.Jam;
import game.peanutpanda.jam.ScreenSize;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        System.setProperty("org.lwjgl.opengl.Window.undecorated", "false");
        config.title = "Scary Room";
        config.width = ScreenSize.WIDTH.getSize();
        config.height = ScreenSize.HEIGHT.getSize();
        config.resizable = true;
        new LwjglApplication(new Jam(), config);
    }
}